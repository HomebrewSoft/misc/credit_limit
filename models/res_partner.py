# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class PartnerCredit(models.Model):
    _inherit = 'res.partner'

    credit_limit = fields.Monetary(
    )
