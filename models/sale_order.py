# -*- coding: utf-8 -*-
import logging

from odoo import _, api, fields, models
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.depends('partner_id', 'amount_total')
    def _compute_credit_availability(self):
        for record in self:
            account_invoice_report = self.env['account.invoice.report']
            all_partner_ids = [record.partner_id.id]
            all_partner_ids += record.partner_id.child_ids.ids
            all_invoices = account_invoice_report.search([
                ('partner_id', 'in', all_partner_ids),
                ('state', 'not in', ['draft', 'cancel']),
                ('type', 'in', ('out_invoice', 'out_refund')),
            ])
            total_invoiced = sum(invoice.residual for invoice in all_invoices)
            _logger.info(_('Total residual: {}').format(total_invoiced))

            # current sale + invoices comparison against partner credit limit
            total_debt = total_invoiced + record.amount_total
            if record.partner_id.credit_limit >= total_debt:
                record.credit_availability = "done"
            else:
                record.credit_availability = "blocked"

    credit_availability = fields.Selection(
        selection=[
            ('normal', 'Unfollowed'),
            ('done', 'Ok'),
            ('blocked', 'Limit reached'),
        ],
        string='Current credit availability',
        readonly=True,
        compute='_compute_credit_availability',
        store=True,
    )

    def action_confirm(self):
        if self.credit_availability == "blocked" and not self.env.user.has_group('credit_limit.credit_limit_group_id'):
            raise UserError(_('The current User is not allowed to continue'))
        result = super(SaleOrder, self).action_confirm()
        return result
