# -*- coding: utf-8 -*-
import logging

from odoo import _, api, fields, models
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    @api.depends('partner_id', 'move_ids_without_package')
    def _compute_credit_availability(self):
        for record in self:
            account_invoice_report = self.env['account.invoice.report']
            all_partner_ids = [record.partner_id.id]
            all_partner_ids += record.partner_id.child_ids.ids
            all_invoices = account_invoice_report.search([
                ('partner_id', 'in', all_partner_ids),
                ('state', 'not in', ['draft', 'cancel']),
                ('type', 'in', ('out_invoice', 'out_refund')),
            ])
            total_invoiced = sum(invoice.residual for invoice in all_invoices)
            _logger.info(_('Total residual: {}').format(total_invoiced))

            # current sale + invoices comparison against partner credit limit
            move_total = 0
            for line in record.move_ids_without_package:
                move_total += line.product_id.list_price * line.product_uom_qty
            total_debt = total_invoiced + move_total
            if record.partner_id.credit_limit >= total_debt:
                record.credit_availability = "done"
            else:
                record.credit_availability = "blocked"

    credit_availability = fields.Selection(
        selection=[
            ('normal', 'Unfollowed'),
            ('done', 'Ok'),
            ('blocked', 'Limit reached'),
        ],
        string='Current credit availability',
        readonly=True,
        compute=_compute_credit_availability,
        store=True,
    )

    def button_validate(self):
        if self.credit_availability == "blocked" and not self.env.user.has_group('credit_limit.credit_limit_group_id'):
            raise UserError(_('The current User is not allowed to continue'))
        result = super(StockPicking, self).button_validate()
        return result
