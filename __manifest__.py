# -*- coding: utf-8 -*-
{
    'name': 'Credit limit',
    'version': '13.0.0.1.0',
    'author': 'HomebrewSoft',
    'website': 'https://homebrewsoft.dev/',
    'license': 'LGPL-3',
    'depends': [
        'contacts',
        'sale_management',
        'stock',
        'account_invoice_analisys_extension',
    ],
    'data': [
        'data/res_groups.xml',
        'views/res_partner.xml',
        'views/sale_order.xml',
        'views/stock_picking.xml',
    ],
}
